library(cAIC4)
data(Orthodont, package = "nlme")
models <- list(
  model1 <- lmer(formula = distance ~ age + Sex + (1 | Subject) + age:Sex,
                 data = Orthodont),
  model2 <- lmer(formula = distance ~ age + Sex + (1 | Subject),
                 data = Orthodont),
  model3 <- lmer(formula = distance ~ age + (1 | Subject),
                 data = Orthodont),
  model4 <- lmer(formula = distance ~ Sex + (1 | Subject),
                 data = Orthodont))

# foo <- getWeights(models = models)
# foo
# 

m             <- models
.envi         <- environment()
# Creation of the variables required for the optimization of weights
# TODO: Suppress anocAIC's automatic output
invisible(capture.output(modelcAIC <- anocAIC(m)))
df            <- modelcAIC[[2]]
tempm         <- m[[which.max(modelcAIC$df)]]
seDF          <- getME(tempm, "sigma")
varDF         <- seDF * seDF
y             <- getME(m[[1]], "y")
mu            <- list()
# TODO: that needs to be made more effective ...
for(i in 1:length(m)){
  mu[[i]]     <- getME(m[[i]], "mu")
}
mu            <- t(matrix(unlist(mu), nrow = length(m), byrow = TRUE))
weights       <- rep(1/length(m), times = length(m))
fun           <- find_weights <- function(w){(t(y - matrix(mu %*% w))%*%(y - matrix(mu %*% w))) + 2 * varDF * (w %*% df)}
eqfun         <- equal <-function(w){sum(w)}
equB          <- 1
lowb          <- rep(0, times = length(m))
uppb          <- rep(1, times = length(m))
nw            <- length(weights)
funv 	        <- find_weights(weights)
eqv 	        <- (sum(weights)-equB)
rho           <- 0
maxit         <- 400
minit         <- 800
delta         <- (1.0e-7)
tol           <- (1.0e-8)
# Start of optimization:
j             <- jh <- funv
lambda        <- c(0)
constraint    <- eqv
p             <- c(weights)
hess          <- diag(nw)
mue           <- nw
.iters        <- 0
targets       <- c(funv, eqv)
# List object for max iters:
majoritervalues <- list()
tic           <- Sys.time()
while( .iters < maxit ){
  .iters <- .iters + 1
  scaler <- c( targets[ 1 ], rep(1, 1) * max( abs(targets[ 2:(1  + 1) ]) ) )
  scaler <- c(scaler, rep( 1, length.out = length(p) ) )
  scaler <- apply( matrix(scaler, ncol = 1), 1,
                   FUN = function( x ) min( max( abs(x), tol ), 1/tol ) )
  # res    <- .weightOptim(weights = p, lm = lambda, targets = targets,
  #                        hess = hess, lambda = mue, scaler = scaler, .envi = .envi)
  res    <- .subnp(pars = p, lm = lambda, ob = targets, hessv = hess, lambda = mu, vscale = scaler, .env = .envi) 
  p      <- res$p
  lambda <- res$y
  hess   <- res$hess
  mue    <- res$lambda
  miniter_values <- res$miniter_values
  temp   <- p
  funv 	 <- find_weights(temp)
  eqv 	 <- (sum(temp)-equB)
  targets     <- c(funv, eqv)
  # Change of the target function through optimization
  tt     <- (j - targets[ 1 ]) / max(targets[ 1 ], 1)
  j      <- targets[ 1 ]
  constraint <- targets[ 2 ]
  if( abs(constraint) < 10 * tol ) {
    rho  <- 0
    mue  <- min(mue, tol)
  }
  if( c( tol + tt ) <= 0 ) {
    lambda <- 0
    hess  <- diag( diag ( hess ) )
  }
  if( sqrt(sum( (c(tt, eqv))^2 )) <= tol ) {
    maxit <- .iters
  }
  majoritervalues[[.iters]] <- c(temp)
}
toc <- Sys.time() - tic
ans <- list(weights = p, functionvalue = j, lagrange = lambda, hessian = hess,
            duration = toc, majoritervalues = majoritervalues, minitervalues = miniter_values)
